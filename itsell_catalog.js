(function($) {


    $.fn.greenify = function(options) {
        
        var settings = $.extend({
            // These are the defaults.
            libload_css: [
            	'https://cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css',
            ],
            libload_js: [
            	'https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js',
            	'https://cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js',
            	],
            libload_AppendSelector: `head`
        }, options );
        
        // load js scripts
        $.each(settings.libload_js, function(index, url) {
            $(settings.libload_AppendSelector).append(`
            <script src="${url}"></script>`)
        })
        
        //load css scripts
        $.each(settings.libload_css, function(index, url) {
            $(settings.libload_AppendSelector).append(`
            <link rel="stylesheet" href="${url}">`)
            console.log(url)
        })
        
        
        
        $("div.category-content-block > div")
        .each(function() {
        //
		const 
		elemName = $(this).find(".product-name"),
		getUrl = $(elemName).children("a").attr("href"),
		getName = $(elemName).children("a").text();
		
		//
		$(elemName).after(`<button data-clipboard-text="${getUrl}" data-title="${getName}" class="btnCopy">Link Copy</button>`);
        });
        
        
        
        $(`<script>
            //
    
        $(".btnCopy").click(function() {
			  //
			  let prodName = $(this).attr("data-title");
			  
			  alertify.success('Copy URL: -- '+prodName+' --');
              alertify.success("done");
			  new ClipboardJS('.btnCopy');
              $(this).attr("disabled", true);
              
			
		})
             //
           </script>`).appendTo("body")
	
        

        
        return this
    };
    
  
})(jQuery); 
 

